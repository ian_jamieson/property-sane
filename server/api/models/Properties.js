/**
 * Properties.js
 *
 * @description :: This is the property model for storing the address, prices, features and images
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    // Main Details
    number: {
      type: 'string',
      required: true
    },
    address_line_1: {
      type: 'string',
      required: true
    },
    address_line_2: {
      type: 'string',
      required: true
    },
    town: {
      type: 'string',
      required: true
    },
    region: {
      type: 'string',
      required: true
    },
    postcode: {
      type: 'string',
      required: true
    },
    country: {
      type: 'string',
      required: true
    },
    // Pricing Details
    price: {
      type: 'float',
      required: true
    },
    price_type: {
      type: 'string',
      required: true
    },
    // Specifics
    bedrooms: {
      type: 'string',
      required: true
    },
    features: {
      type: 'string',
      required: true
    },
    agent_id: {
      type: 'string',
      required: true
    }
  }
};

