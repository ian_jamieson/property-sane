import DS from 'ember-data';

export default DS.Model.extend({
    // Main Details
    number: DS.attr('string'),
    address_line_1: DS.attr('string'),
    address_line_2: DS.attr('string'),
    town: DS.attr('string'),
    region: DS.attr('string'),
    postcode: DS.attr('string'),
    country: DS.attr('string'),
    // Pricing Details
    price: DS.attr('price'),
    price_type: DS.attr('string'),
    // Specifics
    bedrooms: DS.attr('number'),
    features: DS.attr('string'),
    agent_id: DS.attr('number'),
});
